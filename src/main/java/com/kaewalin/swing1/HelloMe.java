/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.swing1;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("MyActionListener: Action");
    }

}

/**
 *
 * @author ACER
 */
public class HelloMe implements ActionListener {
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(300, 200);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourName = new JLabel("Your Name: ");
        lblYourName.setSize(80, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.PINK);
        lblYourName.setOpaque(true);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(80, 20);
        txtYourName.setLocation(90, 5);
        
        
        JButton bthHello = new JButton("Hello");
        bthHello.setSize(80, 20);
        bthHello.setLocation(90, 40);
        
        MyActionListener myActionListener = new MyActionListener();
        bthHello.addActionListener(myActionListener);
        bthHello.addActionListener(new HelloMe());
        
        ActionListener actionListener = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("Anonymous Class: Action");
            }
            
        };
        bthHello.addActionListener(actionListener);
        
        JLabel lblHello = new JLabel("Hello...",JLabel.CENTER);
        lblHello.setSize(80, 20);
        lblHello.setLocation(80,70);
        lblHello.setBackground(Color.PINK);
        lblHello.setOpaque(true);
        
        frmMain.setLayout(null);
        
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(bthHello);
        frmMain.add(lblHello);
        
        frmMain.setVisible(true);
        
          
        bthHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String name = txtYourName.getText();
                lblHello.setText("Hello " + name);
            }
            
        });

    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("HelloMe: Action");
    }
    
    
    
}
